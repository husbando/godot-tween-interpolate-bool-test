extends CenterContainer

export (bool) var truth = true setget set_truth

onready var slider = find_node("Slider")
onready var truth_label = find_node("TruthLabel")

var ease_type = Tween.EASE_IN
var transition_type = Tween.TRANS_LINEAR

func set_truth(value):
	truth = value
	truth_label.text = String(value)

func set_ease_type(type):
	ease_type = type

func set_transition_type(type):
	transition_type = type

func set_to_true():
	$Tween.interpolate_property(slider, "value", 100, 0, 3, transition_type, ease_type)
	$Tween.interpolate_property(self, "truth", false, true, 3)
	$Tween.start()

func set_to_false():
	$Tween.interpolate_property(slider, "value", 0, 100, 3, transition_type, ease_type)
	$Tween.interpolate_property(self, "truth", true, false, 3)
	$Tween.start()
